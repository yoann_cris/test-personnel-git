#!/bin/bash


echo "              WARNING"
echo "        ______________________"
echo "          CHOOSE THE FORK  "
echo "(example : /user/git/fork_clone_git )"

read fork
echo ~/git/$fork
cd ~/git/$fork

while [ 1 ];
do

echo "   GIT UPDATE TOOL"
echo "______________________"
echo "1)  REMOTE LIST"
echo "2)  ADD REMOTE"
echo "3)  UPDATE"
echo "4)  EXIT"

read select

case $select in
    1)
        git remote -v
        ;;
    2)

        echo "INPUT THE ORIGIN MASTER LINK"
        read origin
        git remote add upstream $origin
        echo "added"
        ;;
    3)
        echo "CHOOSE THE BRANCH TO UPDATE"
        read branch

        echo "READ ORIGIN"
        git fetch upstream
        echo "READ ORIGIN = PASS"
        echo "UPDATE LOCAL"
        git pull upstream $branch
        echo "UPDATE LOCAL = PASS"
        echo "UPDATE GIT FORK"
        git push origin $branch
        echo "UPDATE GIT FORK = PASS"
        ;;
    4)
        exit ;;
esac

done;